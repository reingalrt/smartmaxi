import 'package:flutter/material.dart';

class ButtonComponent extends StatelessWidget {
  final double buttonHeight;
  final double buttonWidth;
  final Color color;
  final String text;
  final double radius;
  final TextStyle textStyle;
  final Color borderColor;
  final bool useBorder;
  final Function onTap;
  final String buttonLeadingIcon;

  const ButtonComponent(
      {Key key,
      this.buttonHeight,
      this.buttonWidth,
      this.color,
      this.text,
      this.textStyle,
      this.radius,
      this.borderColor: Colors.transparent,
      this.useBorder,
      this.buttonLeadingIcon: '',
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: buttonHeight,
      width: buttonWidth,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(
              color: borderColor, width: useBorder == false ? 0 : 1)),
      child: Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(radius),
        child: InkWell(
          splashColor: Colors.white.withOpacity(1),
          onTap: onTap,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(buttonLeadingIcon, scale: 2),
                SizedBox(width: 5),
                Text(
              text,
              style: textStyle,
            ),
              ]
            )
          ),
        ),
      ),
    );
  }
}
