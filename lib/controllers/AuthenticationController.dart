import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart' as crypto;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smartmaxi/helpers/API.dart';

class AuthenticationController{
  ApiManager apiManager = new ApiManager();

  Future<http.Response> login(String username, String password) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();

    String url = apiManager.baseApi + apiManager.loginApi;

    //convert the publicKey string based on "swirf" + username into md5, also hash the password too
    final publicKey =
        crypto.md5.convert(utf8.encode("swirf$username")).toString();
    final hashedPassword = crypto.md5.convert(utf8.encode(password)).toString();


    final response = await http.post(
      url,
      body: {
        'action': 'SIGNON',
        'username': username,
        'public_key': publicKey,
        'password': hashedPassword
      }
    );

    return response;
  }
}