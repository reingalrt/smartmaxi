//This project was made by digidaw for Swirf company

import 'package:flutter/material.dart';
import 'package:smartmaxi/helpers/ColorsManager.dart';
import 'package:smartmaxi/widgets/CompleteLogin.dart';
import 'package:smartmaxi/widgets/DashboardMenu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
	    debugShowCheckedModeBanner: false,
      title: 'SMARTMAXI',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController usernameController = TextEditingController();

	ColorsManager colorsManager = new ColorsManager();
	Color textColor;

	@override
  void initState() {
    textColor = colorsManager.smartmaxiGrey;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
	    resizeToAvoidBottomPadding: false,
      body: ListView(
	      shrinkWrap: true,
		      children: <Widget>[
		      	Container(
			        height: 207 - 66.0,
			        width: MediaQuery.of(context).size.width,
			        decoration: BoxDecoration(
				        image: DecorationImage(
					        image: AssetImage('assets/images/ellipse.png'),
					        fit: BoxFit.fill
				        )
			        ),
			        child: Center(
				        child: Image.asset('assets/images/company_name.png', height: 16, width: 184,),
			        ),
		        ),
			      SizedBox(height: 34,),
			      Center(
			        child: Text(
				      'SIGN IN TO YOUR ACCOUNT',
				      style: TextStyle(color: colorsManager.smartmaxiBlue, fontSize: 18, fontWeight: FontWeight.w500),
			        ),
			      ),
		        SizedBox(height: 26,),
			      Container(
				      margin: EdgeInsets.symmetric(horizontal: 49),
				      height: 62,
				      width: 262,
				      decoration: BoxDecoration(
					      color: Colors.white,
					      borderRadius: BorderRadius.circular(10),
					      boxShadow: <BoxShadow>[
					      	BoxShadow(
						        spreadRadius: 1.5,
						        blurRadius: 2,
						        color: Colors.black.withOpacity(0.1),
						        offset: Offset(0, 1)
					        )
					      ]
				      ),
				      child: Material(
					      borderRadius: BorderRadius.circular(10),
				        child: InkWell(
					      borderRadius: BorderRadius.circular(10),
					      highlightColor: Colors.blue,
				          onTap: (){

				          },
				          child: Row(
					      mainAxisAlignment: MainAxisAlignment.center,
					      children: <Widget>[
							Image.asset('assets/icons/google.png', height: 32, width: 32,),
						      SizedBox(
							      width: 8,
						      ),
						      Text(
							      'Sign in With Google',
							      style: TextStyle(
								      color: textColor,
								      fontSize: 12,
								      fontWeight: FontWeight.w500
							      ),
						      )
					      ],
				          ),
				        ),
				      ),
			      ),
			      SizedBox(
				      height: 16,
			      ),
			      Center(child: Text('OR', style: TextStyle(color: colorsManager.smartmaxiGrey),)),
		        SizedBox(
			        height: 16,
		        ),
			      Container(
				      margin: EdgeInsets.symmetric(horizontal: 49),
				      height: 62,
				      width: 262,
				      decoration: BoxDecoration(
					      color: Colors.white,
					      borderRadius: BorderRadius.circular(10),
					      boxShadow: <BoxShadow>[
						      BoxShadow(
							      spreadRadius: 1.5,
							      blurRadius: 2,
							      color: Colors.black.withOpacity(0.1),
							      offset: Offset(0, 1)
						      )
					      ]
				      ),
				      child: TextFormField(
                controller: usernameController,
					      decoration: InputDecoration(
						      hintText: 'Use Your Email As User Name',
						      contentPadding: EdgeInsets.only(top: 50, left: 20),
						      enabledBorder: OutlineInputBorder(
							      borderSide: BorderSide.none,
						      ),
						      focusedBorder: OutlineInputBorder(
							      borderSide: BorderSide.none
						      )
					      ),
				      ),
			      ),
			      SizedBox(
				      height: 43,
			      ),
			      Container(
				      margin: EdgeInsets.symmetric(horizontal: 49),
				      height: 62,
				      width: 262,
				      decoration: BoxDecoration(
					      color: colorsManager.smartmaxiGreen,
					      borderRadius: BorderRadius.circular(10),
					      boxShadow: <BoxShadow>[
						      BoxShadow(
							      spreadRadius: 1.5,
							      blurRadius: 2,
							      color: Colors.black.withOpacity(0.1),
							      offset: Offset(0, 1)
						      )
					      ]
				      ),
				      child: Material(
					      color: colorsManager.smartmaxiGreen,
					      borderRadius: BorderRadius.circular(10),
					      child: InkWell(
                  highlightColor: Colors.blue,
						      splashColor: Colors.white,
						      onTap: ()async{
                    await Future.delayed(Duration(seconds: 1));
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => CompleteLogin(username: usernameController.text,)
                    ));
                  },
						      child: Center(
							      child: Text('Continue', style: TextStyle(color: Colors.white),),
						      ),
					      ),
				      ),
			      ),
		        SizedBox(
			        height: 22,
		        ),
			      Center(child: Text('problems with signing in?', style: TextStyle(fontSize: 10, color: colorsManager.smartmaxiGrey),)),
		        SizedBox(
			        height: 1,
		        ),
		        Center(child: Text('don\'t worry, click here!', style: TextStyle(fontSize: 10, color: colorsManager.smartmaxiBlue),)),
		        SizedBox(
			        height: 36,
		        ),
		        Center(child: Text('Create a SmartMaxi Account', style: TextStyle(fontSize: 12, color: colorsManager.smartmaxiBlue),))
		      ],
	      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
