import 'package:flutter/material.dart';

class ColorsManager{
	Color smartmaxiGrey = Color(0xff828C8C);
	Color smartmaxiGreen = Color(0xff1BBD18);
	Color smartmaxiBlue = Color(0xff204D8C);
  Color smartmaxiStrongGrey = Color(0xffE0E7F0);
  Color smartmaxiBlueSea = Color(0xff58A4B0);
}