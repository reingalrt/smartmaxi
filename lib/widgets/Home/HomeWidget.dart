import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:smartmaxi/Components/ButtonComponent.dart';
import 'package:smartmaxi/helpers/ColorsManager.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class HomeWidget extends StatefulWidget {
  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget>
    with SingleTickerProviderStateMixin {
  ColorsManager colorsManager = ColorsManager();
  List<charts.Series> seriesList;
  bool animate;
  bool showHistory = false;
  bool showTransactionHistory = false;
  Animation<double> animation;
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    setState(() {
      seriesList = _createSampleData();
      animate = true;
      animationController = AnimationController(
          duration: const Duration(seconds: 0), vsync: this);
      animation = Tween<double>(begin: 0, end: 150).animate(new CurvedAnimation(
          parent: animationController,
          curve: Curves.easeIn,
          reverseCurve: Curves.easeOut));
      animationController.forward();
    });
  }

  static List<charts.Series<LinearSales, int>> _createSampleData() {
    final data = [
      new LinearSales(2, 40),
      new LinearSales(3, 32),
      new LinearSales(4, 31),
      new LinearSales(5, 15),
      new LinearSales(6, 10),
      new LinearSales(7, 8),
      new LinearSales(8, 16),
      new LinearSales(9, 18),
      new LinearSales(10, 15),
      new LinearSales(11, 2),
      new LinearSales(12, 5),
      new LinearSales(13, 2),
      new LinearSales(14, 1),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    print(45 * 3);
    return SafeArea(
      child: Container(
        height: 520,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            bankAccountDetailWidget(),
            balanceHistoryWidget(),
            utilityButtonWidget(),
            showTransactionHistory == false
                ? smartmaxiAccountListWidget()
                : transactionHistoryWidget()
          ],
        ),
      ),
    );
  }

  Widget bankAccountDetailWidget() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
      height: 40,
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Bank Account:',
                style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                    color: colorsManager.smartmaxiGrey),
              ),
              Text(
                'LT 12 10000 11101001002 [EUR]',
                style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.bold,
                    color: colorsManager.smartmaxiBlue),
              ),
              Text(
                'Bank Name 1',
                style: TextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.normal,
                    color: colorsManager.smartmaxiBlue),
              ),
            ],
          ),
          SizedBox(
            width: 50,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'EUR 1 000,00',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: colorsManager.smartmaxiBlue),
              ),
              Text(
                'Total available: EUR 4 250,00',
                style: TextStyle(
                    fontSize: 8,
                    fontWeight: FontWeight.normal,
                    color: colorsManager.smartmaxiBlueSea),
              ),
            ],
          )
        ],
      ),
    );
  }

  

  Widget balanceHistoryWidget() {
    return GestureDetector(
      onTap: () {
        setState(() {
          showHistory = !showHistory;
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 23, vertical: 21),
        margin: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  spreadRadius: 1.5,
                  blurRadius: 2,
                  offset: Offset(0, 1),
                  color: Colors.black.withOpacity(.2)),
            ]),
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'Your Balance History',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: colorsManager.smartmaxiGrey,
                      fontSize: 10),
                ),
                Expanded(
                  child: SizedBox(),
                ),
                Image.asset('assets/icons/chart.png', scale: 2)
              ],
            ),
            showHistory == false
                ? Container()
                : Container(
                    height: animation.value,
                    child: charts.LineChart(
                      seriesList,
                      animate: animate,
                    ),
                  )
          ],
        ),
      ),
    );
  }

  Widget utilityButtonWidget() {
    return Container(
      height: 40,
      padding: EdgeInsets.symmetric(vertical: 2),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 21),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ButtonComponent(
            buttonLeadingIcon: 'assets/icons/arrow_up_dashed.png',
            buttonHeight: 38,
            buttonWidth: 97,
            useBorder: false,
            color: colorsManager.smartmaxiBlueSea,
            text: 'TOP UP',
            radius: 10,
            textStyle: TextStyle(
                color: Colors.white, fontSize: 9, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 11,
          ),
          ButtonComponent(
            buttonLeadingIcon: 'assets/icons/transfer.png',
            buttonHeight: 38,
            buttonWidth: 97,
            useBorder: false,
            color: colorsManager.smartmaxiBlueSea,
            text: 'TRANSFER',
            radius: 10,
            textStyle: TextStyle(
                color: Colors.white, fontSize: 9, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 11,
          ),
          ButtonComponent(
            buttonLeadingIcon: showTransactionHistory == true ? '' : 'assets/icons/history.png',
            buttonHeight: 38,
            buttonWidth: 97,
            useBorder: false,
            color: colorsManager.smartmaxiBlue,
            text: showTransactionHistory == true ? 'BANK ACCOUNTS' : 'SEE HISTORY',
            radius: 10,
            onTap: () {
              setState(() {
                showTransactionHistory = !showTransactionHistory;
              });
            },
            textStyle: TextStyle(
                color: Colors.white, fontSize: 9, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }

  Widget smartmaxiAccountListWidget() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 30),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Text(
            'All Your SmartMaxi Accounts:',
            style: TextStyle(color: colorsManager.smartmaxiGrey, fontSize: 10),
          ),
          SizedBox(
            height: 14,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Bank Name 1',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      'EUR 1 000,00',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiBlue),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: <Widget>[
                    Text('LT 12 10000 11101001000 ',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
                    Text(
                      '[EUR]',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    )
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Text(
                  'Top test',
                  style: TextStyle(
                      fontSize: 10, color: colorsManager.smartmaxiGrey),
                ),
                SizedBox(
                  height: 7,
                ),
                Divider()
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Bank Name 1',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      'EUR 1 000,00',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiBlue),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: <Widget>[
                    Text('LT 12 10000 11101001000 ',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
                    Text(
                      '[EUR]',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    )
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Text(
                  'Top test',
                  style: TextStyle(
                      fontSize: 10, color: colorsManager.smartmaxiGrey),
                ),
                SizedBox(
                  height: 7,
                ),
                Divider()
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Bank Name 1',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      'EUR 1 000,00',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiBlue),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: <Widget>[
                    Text('LT 12 10000 11101001000 ',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
                    Text(
                      '[EUR]',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    )
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Text(
                  'Top test',
                  style: TextStyle(
                      fontSize: 10, color: colorsManager.smartmaxiGrey),
                ),
                SizedBox(
                  height: 7,
                ),
                Divider()
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Bank Name 1',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      'USD 0,00',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiBlue),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: <Widget>[
                    Text('LT 12 10000 11101001000 ',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
                    Text(
                      '[USD]',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiGrey),
                    )
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Text(
                  'Top test',
                  style: TextStyle(
                      fontSize: 10, color: colorsManager.smartmaxiGrey),
                ),
                SizedBox(
                  height: 7,
                ),
                Divider()
              ],
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: ButtonComponent(
              color: Colors.white,
              buttonHeight: 38,
              buttonWidth: 152,
              radius: 10,
              useBorder: true,
              borderColor: colorsManager.smartmaxiBlue,
              text: '+ Add Bank Account',
              textStyle:
                  TextStyle(color: colorsManager.smartmaxiBlue, fontSize: 9),
            ),
          )
        ],
      ),
    );
  }

  Widget transactionHistoryWidget() {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 30),
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Text(
            'Your History:',
            style: TextStyle(color: colorsManager.smartmaxiGrey, fontSize: 10),
          ),
          SizedBox(
            height: 14,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            leading: CircleAvatar(
              child: Image.asset('assets/icons/shop.png', scale: 1.5),
            ),
            title: Row(
                  children: <Widget>[
                    Text(
                      'Shop',
                      style: TextStyle(
                          fontSize: 10, fontWeight: FontWeight.bold, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      '-EUR 100,00',
                      style: TextStyle(
                          fontSize: 10, color: Colors.red),
                    ),
                  ],
                ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                    Text('6 August 2019, 01:54:37 PM',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
              ],
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Divider(),
          SizedBox(
            height: 7,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            leading: CircleAvatar(
              child: Image.asset('assets/icons/cash.png', scale: 1.5),
            ),
            title: Row(
                  children: <Widget>[
                    Text(
                      'Cash Withdrawal',
                      style: TextStyle(
                          fontSize: 10, fontWeight: FontWeight.bold, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      '-EUR 300,00',
                      style: TextStyle(
                          fontSize: 10, color: Colors.red),
                    ),
                  ],
                ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                    Text('6 August 2019, 01:54:37 PM',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
              ],
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Divider(),
          SizedBox(
            height: 7,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            leading: CircleAvatar(
              child: Image.asset('assets/icons/arrow_up_dashed.png', scale: 1.5),
            ),
            title: Row(
                  children: <Widget>[
                    Text(
                      'Top Up',
                      style: TextStyle(
                          fontSize: 10, fontWeight: FontWeight.bold, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      '+EUR 800,00',
                      style: TextStyle(
                          fontSize: 10, color: colorsManager.smartmaxiBlue),
                    ),
                  ],
                ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                    Text('6 August 2019, 01:54:37 PM',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
              ],
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Divider(),
          SizedBox(
            height: 7,
          ),
          ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: 0,
            ),
            leading: CircleAvatar(
              child: Image.asset('assets/icons/cart.png', scale: 1.5),
            ),
            title: Row(
                  children: <Widget>[
                    Text(
                      'Groceries',
                      style: TextStyle(
                          fontSize: 10, fontWeight: FontWeight.bold, color: colorsManager.smartmaxiGrey),
                    ),
                    Expanded(
                      child: SizedBox(),
                    ),
                    Text(
                      '-EUR 800,00',
                      style: TextStyle(
                          fontSize: 10, color: Colors.red),
                    ),
                  ],
                ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                
                    Text('6 August 2019, 01:54:37 PM',
                        style: TextStyle(
                            fontSize: 10, color: colorsManager.smartmaxiBlue)),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// class AnimatedLinearChartHeight extends AnimatedWidget {
//   final List<charts.Series> seriesList;
//   final animate;

//   AnimatedLinearChartHeight({Key key, Animation<double> animation, this.seriesList, this.animate: false, })
//       : super(key: key, listenable: animation);

//   @override
//   Widget build(BuildContext context) {
//     final animation = listenable as Animation<double>;

//     return
//   }
// }

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
