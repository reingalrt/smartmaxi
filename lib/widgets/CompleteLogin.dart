import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:smartmaxi/controllers/AuthenticationController.dart';
import 'package:smartmaxi/helpers/ColorsManager.dart';
import 'package:smartmaxi/widgets/DashboardMenu.dart';

class CompleteLogin extends StatefulWidget {
  final username;

  const CompleteLogin({Key key, this.username}) : super(key: key);

  @override
  _CompleteLoginState createState() => _CompleteLoginState();
}

class _CompleteLoginState extends State<CompleteLogin> {
  TextEditingController userNameController;
  TextEditingController passwordController;

  ColorsManager colorsManager = new ColorsManager();
  Color textColor;

  AuthenticationController authenticationController =
      new AuthenticationController();

  @override
  void initState() {
    textColor = colorsManager.smartmaxiGrey;
    setState(() {
      userNameController = TextEditingController(text: widget.username);
      passwordController = TextEditingController();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Container(
            height: 207 - 66.0,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/ellipse.png'),
                    fit: BoxFit.fill)),
            child: Center(
              child: Image.asset(
                'assets/images/company_name.png',
                height: 16,
                width: 184,
              ),
            ),
          ),
          SizedBox(
            height: 34,
          ),
          Center(
            child: Text(
              'SIGN IN TO YOUR ACCOUNT',
              style: TextStyle(
                  color: colorsManager.smartmaxiBlue,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 26,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 49),
            height: 62,
            width: 262,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      spreadRadius: 1.5,
                      blurRadius: 2,
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(0, 1))
                ]),
            child: TextFormField(
              controller: userNameController,
              decoration: InputDecoration(
                  hintText: 'Use Your Email As User Name',
                  contentPadding: EdgeInsets.only(top: 50, left: 20),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder:
                      OutlineInputBorder(borderSide: BorderSide.none)),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 49),
            height: 62,
            width: 262,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      spreadRadius: 1.5,
                      blurRadius: 2,
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(0, 1))
                ]),
            child: TextFormField(
              controller: passwordController,
              decoration: InputDecoration(
                  hintText: 'Password',
                  contentPadding: EdgeInsets.only(top: 50, left: 20),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder:
                      OutlineInputBorder(borderSide: BorderSide.none)),
            ),
          ),
          SizedBox(
            height: 43,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 49),
            height: 62,
            width: 262,
            decoration: BoxDecoration(
                color: colorsManager.smartmaxiGreen,
                borderRadius: BorderRadius.circular(10),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      spreadRadius: 1.5,
                      blurRadius: 2,
                      color: Colors.black.withOpacity(0.1),
                      offset: Offset(0, 1))
                ]),
            child: Material(
              color: colorsManager.smartmaxiGreen,
              borderRadius: BorderRadius.circular(10),
              child: InkWell(
                highlightColor: Colors.blue,
                splashColor: Colors.white,
                onTap: () async {
                  authenticationController
                      .login(userNameController.text, passwordController.text)
                      .then((response) {
                    print(response.statusCode);
                    print(response.body);

                    var extractedData = json.decode(response.body);

                    if (response.statusCode == 200 || response.statusCode == 201) {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DashboardMenu()));
                    }
                  });
                },
                child: Center(
                  child: Text(
                    'SIGN IN',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 22,
          ),
          Center(
              child: Text(
            'problems with signing in?',
            style: TextStyle(fontSize: 10, color: colorsManager.smartmaxiGrey),
          )),
          SizedBox(
            height: 1,
          ),
          Center(
              child: Text(
            'don\'t worry, click here!',
            style: TextStyle(fontSize: 10, color: colorsManager.smartmaxiBlue),
          )),
          SizedBox(
            height: 36,
          ),
          Center(
              child: Text(
            'Create a SmartMaxi Account',
            style: TextStyle(fontSize: 12, color: colorsManager.smartmaxiBlue),
          ))
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
