import 'package:flutter/material.dart';

class ChatLobby extends StatefulWidget {
  @override
  _ChatLobbyState createState() => _ChatLobbyState();
}

class _ChatLobbyState extends State<ChatLobby> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 23, vertical: 21),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  spreadRadius: 1.5,
                  blurRadius: 2,
                  offset: Offset(0, 1),
                  color: Colors.black.withOpacity(.2)),
            ]),
        child: Row(
          children: <Widget>[
            Icon(Icons.perm_identity),
            SizedBox(width: 8,),
            Text('User 1'),
          ],
        )
        )
      ]
    );
  }
}