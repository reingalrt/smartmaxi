import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:smartmaxi/Components/ButtonComponent.dart';
import 'package:smartmaxi/Components/FabBottomAppBarItem.dart';
import 'package:smartmaxi/helpers/ColorsManager.dart';
import 'package:smartmaxi/widgets/Chat/ChatLobby.dart';
import 'package:smartmaxi/widgets/Home/HomeWidget.dart';

class DashboardMenu extends StatefulWidget {
  @override
  _DashboardMenuState createState() => _DashboardMenuState();
}

class _DashboardMenuState extends State<DashboardMenu> {
  ColorsManager colorsManager = new ColorsManager();

  int _selectedPage = 0;

  String _scannedPayload;

  List<Widget> pageList = [
    HomeWidget(),
    ChatLobby(),
    Container(
      child: Center(
        child: Text('Item is Empty', style: TextStyle(color: Colors.black))
      )
    ),
    Container(),
  ];

  Future<void> scanSomething() async {
    String scannedPayload;

    try {
      scannedPayload =
          await FlutterBarcodeScanner.scanBarcode("#e5322b", "Cancel", true, ScanMode.QR);
    } catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scannedPayload = scannedPayload;
    });

    print(_scannedPayload);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: colorsManager.smartmaxiBlue,
          elevation: 0,
          centerTitle: true,
          leading: Icon(
            Icons.menu,
            color: Colors.white,
            size: 25,
          ),
          title: Image.asset(
            'assets/images/company_name.png',
            height: 16,
            width: 184,
          ),
          actions: <Widget>[
            Image.asset(
              'assets/icons/robot.png',
              height: 36,
              width: 36,
            ),
            SizedBox(
              width: 12,
            ),
          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            scanSomething();
          },
          tooltip: 'Increment',
          child: Image.asset('assets/icons/qr_icons.png', scale: 2),
          elevation: 2.0,
        ),
        bottomNavigationBar: FABBottomAppBar(
          notchedShape: CircularNotchedRectangle(),
          onTabSelected: (i) {
            setState(() {
              _selectedPage = i;
            });
            print('index of $i');
          },
          items: [
            FABBottomAppBarItem(
                iconData: Icon(
                  Icons.home,
                  color: colorsManager.smartmaxiBlue,
                ),
                text: ''),
                FABBottomAppBarItem(
                iconData: Image.asset(
                  'assets/icons/chat.png',
                  height: 25,
                  width: 25,
                ),
                text: ''),
            FABBottomAppBarItem(
                iconData:
                    Image.asset('assets/icons/transfer.png', height: 25,
                  width: 25, color: colorsManager.smartmaxiBlue),
                text: ''),
            FABBottomAppBarItem(
                iconData: Icon(Icons.people_outline,
                    color: colorsManager.smartmaxiBlue),
                text: ''),
          ],
        ),
        body: ListView(
          children: <Widget>[
          pendingOutgoingWidget(),
          pageList[_selectedPage]
        ]),
      ),
    );
  }

  Widget pendingOutgoingWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 19),
      height: 34,
      color: colorsManager.smartmaxiStrongGrey,
      child: Row(
        children: <Widget>[
          Text(
            'Pending outgoing transactions',
            style: TextStyle(fontSize: 10, color: colorsManager.smartmaxiGrey),
          ),
          Expanded(
            child: Container(),
          ),
          ButtonComponent(
            buttonHeight: 17,
            useBorder: false,
            buttonWidth: 89,
            radius: 10,
            color: colorsManager.smartmaxiBlueSea,
            text: 'APPROVE NOW',
            textStyle: TextStyle(fontSize: 9, color: Colors.white),
          ),
        ],
      ),
    );
  }

  // BottomNavigationBar(
  //         currentIndex: _selectedPage,
  //         selectedIconTheme:
  //             IconThemeData(color: colorsManager.smartmaxiBlue, size: 30),
  //         unselectedIconTheme:
  //             IconThemeData(color: colorsManager.smartmaxiBlue),
  //         onTap: (int index) {
  //           setState(() {
  //             _selectedPage = index;
  //           });
  //         },
  //         items: [
  //           BottomNavigationBarItem(icon: Icon(Icons.home), title: Container()),
  //           BottomNavigationBarItem(
  //               icon: Icon(Icons.assignment), title: Container()),
  //           BottomNavigationBarItem(
  //               icon: Image.asset(
  //                 'assets/icons/chat.png',
  //                 height: 25,
  //                 width: 25,
  //               ),
  //               title: Container()),
  //           BottomNavigationBarItem(
  //               icon: Icon(Icons.people_outline), title: Container())
  //         ],
  //       ),
}
